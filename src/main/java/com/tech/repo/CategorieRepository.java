package com.tech.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.tech.entity.Categorie;

public interface CategorieRepository extends CrudRepository<Categorie, Long> {

    List<Categorie> findByNom(String nom);

    Categorie findById(long categorie_id);
}