package com.tech.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

public class Magasin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long magasin_id;
    private String ville;
    private Date updatedAt;

    @OneToMany
    private Inventaire inventaire;

    @OneToMany
    private Employe directeur;

    @ManyToOne
    private Employe employe;
}
