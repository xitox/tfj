package com.tech.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

public class Inventaire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long inventaire_id;
    private Date updatedAt;

    @ManyToOne
    private Materiel materiel;

    @ManyToOne
    private Magasin magasin;

}
