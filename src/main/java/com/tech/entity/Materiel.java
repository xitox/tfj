package com.tech.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

public class Materiel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long materiel_id;
    private String nom;
    private String description;
    private String marque;
    private String image;
    private String taille;
    private int duree_location;
    private double cout_location;
    private double cout_remplacement;
    private Date updatedAt;

    @ManyToOne
    private Categorie categorie;

    @OneToMany
    private Inventaire inventaire;
}
