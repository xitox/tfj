package com.tech.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Employe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employe_id;
    private String nom;
    private String prenom;
    private String image;
    private String mail;
    private String login;
    private String mdp;
    private boolean actif;
    private Date updatedAt;

    @ManyToOne
    private Magasin directeur;

    @OneToMany
    private Magasin magasin;
}
