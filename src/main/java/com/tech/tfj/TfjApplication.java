package com.tech.tfj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.tech.controller.TestController;

@SpringBootApplication
@ComponentScan(basePackageClasses = TestController.class)
public class TfjApplication {

	public static void main(String[] args) {
		SpringApplication.run(TfjApplication.class, args);
	}

}
