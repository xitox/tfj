package com.tech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tech.entity.Categorie;
import com.tech.repo.CategorieRepository;

@RestController
public class TestController {
    @Autowired
    CategorieRepository cateRepo;

    @GetMapping("/")
    public String index() {
        return "Tfj is working!";
    }

    @PostMapping("categorie/add")
    public Categorie addCate(Categorie categorie) {
        return cateRepo.save(categorie);
    }

    @DeleteMapping("categorie")
    public void deleteCate(Categorie categorie) {
        cateRepo.delete(categorie);
    }

    @PostMapping("categorie/upadate")
    public Categorie updateCate(Categorie categorie, int categorie_id) {
        Categorie old_cate = cateRepo.findById(categorie_id);
        if (old_cate == null) {
            return null;
        } else {
            return cateRepo.save(categorie);
        }
    }
}
